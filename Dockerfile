# ETHZ Notebook with TEOS-10 Extension
FROM registry.ethz.ch/eduit/images/notebooks/jh-notebook-universal:5.2.1-01

USER root

RUN conda install netCDF4 xarray gsw

USER 1000
